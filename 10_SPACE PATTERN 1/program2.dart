/*
       1
     2 3
   4 5 6
7 8 9 10
   
*/
import 'dart:io';

void main() {
  stdout.write("enter no rows : ");
  int n = int.parse(stdin.readLineSync()!);
  int x = 1;
  for (int i = 1; i <= n; i++) {
    for (int s = 1; s <= n - i; s++) {
      stdout.write("  ");
    }
    for (int j = 1; j <= i; j++) {
      stdout.write("$x ");
      x++;
    }
    print("");
  }
}
