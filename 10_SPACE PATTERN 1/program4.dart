/*
       4
     4 3
   4 3 2
 4 3 2 1
   
*/
import 'dart:io';

void main() {
  stdout.write("enter no rows : ");
  int n = int.parse(stdin.readLineSync()!);

  for (int i = 1; i <= n; i++) {
    int x = n;
    for (int s = 1; s <= n - i; s++) {
      stdout.write("  ");
    }
    for (int j = 1; j <= i; j++) {
      stdout.write("$x ");
      x--;
    }
    print("");
  }
}
