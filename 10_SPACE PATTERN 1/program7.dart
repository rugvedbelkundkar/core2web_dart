/*
         1
       4 2
     9 6 3
 16 12 8 4
   
*/
import 'dart:io';

void main() {
  stdout.write("enter no rows : ");
  int n = int.parse(stdin.readLineSync()!);
  
  for (int i = 1; i <= n; i++) {
    int x = i*i;
    for (int s = 1; s <= n - i; s++) {
      stdout.write("  ");
    }
    for (int j = 1; j <= i; j++) {
      stdout.write("$x ");
      x = x - i;
    }
    print("");
  }
}
