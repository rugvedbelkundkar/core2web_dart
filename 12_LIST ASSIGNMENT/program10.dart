import 'dart:io';

void main() {
  var original = [];
  var reversed = [];
  stdout.write('Enter the length of the list: ');
  int length = int.parse(stdin.readLineSync()!);

  for (int i = 0; i < length; i++) {
    stdout.write('Enter the element at index ${i + 1}: ');
    original.add(stdin.readLineSync()!);
  }

  reversed = List.from(original.reversed);

  print('Original List: $original');
  print('Reversed List: $reversed');
}
