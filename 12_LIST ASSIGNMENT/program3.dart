import 'dart:io';
void main() {
   var list = [];
  for (int i = 1; i <= 6; i++) {
    stdout.write ("Enter $i number : ");
    int n = int.parse(stdin.readLineSync()!);
    list.add(n);
  }
  bool div = true;
  for (int i = 0; i < list.length; i++) {
    if (!(list[i] % 3 == 0 && list[i] % 5 == 0)) {
      div = false;
      break;
    }
  }
  print(div);
}

