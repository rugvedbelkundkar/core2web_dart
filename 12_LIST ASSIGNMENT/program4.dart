import 'dart:io';

void main() {
  var list = [];
  stdout.write("enter length of list : ");
  int length = int.parse(stdin.readLineSync()!);
  for (int i = 0; i < length; i++) {
    stdout.write("Enter the number at ${i + 1} index : ");
    int n = int.parse(stdin.readLineSync()!);
    if (i % 2 == 0)
      list.add(n + 3);
    else
      list.add(n);
  }
  print(list);
}
