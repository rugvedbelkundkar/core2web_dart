import 'dart:io';

void main() {
  var list = [];
  stdout.write("enter the lenght of the list : ");
  int length = int.parse(stdin.readLineSync()!);
  for (int i = 0; i < length; i++) {
    stdout.write('Enter element at ${i + 1} index : ');
    int n = int.parse(stdin.readLineSync()!);
    list.add(n * n);
  }
  print(list);
}

