import 'dart:io';

void main() {
  var list = [];
  stdout.write('enter length of the list : ');
  int length = int.parse(stdin.readLineSync()!);
  for (int i = 0; i < length; i++) {
    stdout.write('enter the element at index ${i + 1} : ');
    list.add(int.parse(stdin.readLineSync()!));
  }
  int largest = list[0];
    for (int i = 0; i < length; i++) {
      if (list[i] > largest) {
        largest = list[i];
      }
    }
    print('The largest element in the list is: $largest');
 }
