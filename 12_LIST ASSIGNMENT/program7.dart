import 'dart:io';

void main() {
  var list = [];
  stdout.write('enter length of the list : ');
  int length = int.parse(stdin.readLineSync()!);
  for (int i = 0; i < length; i++) {
    stdout.write('enter the element at index ${i + 1} : ');
    list.add(int.parse(stdin.readLineSync()!));
  }
  stdout.write("enter a number to find in a list : ");
  int num = int.parse(stdin.readLineSync()!);
  if (list.contains(num)) {
    print("$num is present in the list");
  } else {
    print("$num is not present in the list");
  }
}
