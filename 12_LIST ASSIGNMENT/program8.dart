import 'dart:io';

void main() {
  var list = [];
  stdout.write('enter length of the list : ');
  int length = int.parse(stdin.readLineSync()!);
  for (int i = 0; i < length; i++) {
    stdout.write('enter the element at index ${i + 1} : ');
    list.add(int.parse(stdin.readLineSync()!));
  }
  stdout.write('Enter the index to add the number: ');
  int index = int.parse(stdin.readLineSync()!);
  stdout.write('Enter the number to replace : ');
  int number = int.parse(stdin.readLineSync()!);

  if (index >= 0 && index < list.length) {
    list[index] = number;
    print('List after replacement: $list');
  } else {
    print('Invalid index');
  }
}
