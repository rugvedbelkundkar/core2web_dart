import 'dart:io';

void main() {
  var list1 = [];
  var list2 = [];
  var list3 = [];
  stdout.write("enter the length of the list : ");
  int length = int.parse(stdin.readLineSync()!);

  for (int i = 0; i < length; i++) {
    stdout.write("enter the element for list 1 at the index ${i + 1} : ");
    list1.add(int.parse(stdin.readLineSync()!));
  }
  stdout.write("-----------------------------------------");
  for (int i = 0; i < length; i++) {
    stdout.write("\nenter the element for list 2 at the index ${i + 1} : ");
    list2.add(int.parse(stdin.readLineSync()!));
  }
  stdout.write("-----------------------------------------");
  for (int i = 0; i < length; i++) {
    list3.add(list1[i] + list2[i]);
  }
  print("\nThe summed list is : $list3");
}
