import 'dart:io';

void main() {
  stdout.write("Enter the units consumed: ");
  int? units = int.parse(stdin.readLineSync()!);
  int totalBill = 0;

  if (units <= 90) {
    totalBill = 0;
  } else if (units > 90 && units <= 180) {
    totalBill = units * 6;
  } else if (units > 180 && units <= 250) {
    totalBill = units * 10;
  } else {
    totalBill = units * 15;
  }

  print("Total Electricity Bill: $totalBill");
}
