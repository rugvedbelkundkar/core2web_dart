void main() {
  int num1 = 5;
  int num2 = -9;
  if (num1 > 0) {
    print("$num1 is positive");
  }
  if (num2 < 0) {
    print("$num2 is negative");
  }
}
