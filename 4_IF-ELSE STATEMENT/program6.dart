import 'dart:io';

void main() {
  print("Enter a number between 0 to 5: ");
  int? n = int.parse(stdin.readLineSync()!);
  if (n == 0) {
    print("zero");
  } else if (n == 1) {
    print("one");
  } else if (n == 2) {
    print("two");
  } else if (n == 3) {
    print("three");
  } else if (n == 4) {
    print("four");
  } else if (n == 5) {
    print("five");
  } else {
    print("Number is greater than five");
  }
}
