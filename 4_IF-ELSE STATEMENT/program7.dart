import 'dart:io';

void main() {
  stdout.write("Enter the number of month (1 to 12) : ");
  int month = int.parse(stdin.readLineSync()!);
  if (month == 1 ||
      month == 3 ||
      month == 5 ||
      month == 7 ||
      month == 8 ||
      month == 10 ||
      month == 12) {
    print("$month : has 31 days");
  } else if (month == 4 || month == 6 || month == 9 || month == 11) {
    print("$month : has 30 days");
  } else
    print("invalid month");
}
