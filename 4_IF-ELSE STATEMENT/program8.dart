import 'dart:io';

void main() {
  stdout.write("Enter a number: ");
  int? number = int.parse(stdin.readLineSync()!);

  if (number % 3 == 0 && number % 5 == 0) {
    print("Divisible by both 3 and 5");
  } else if (number % 3 == 0) {
    print("Divisible by 3");
  } else if (number % 5 == 0) {
    print("Divisible by 5");
  } else {
    print("Not divisible by 3 or 5");
  }
}
