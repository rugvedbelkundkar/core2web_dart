import 'dart:io';

void main() {
  stdout.write("Enter the ticket type: ");
  int? ticketType = int.parse(stdin.readLineSync()!);

  if (ticketType == 1) {
    print("Please pay 2000");
  } else if (ticketType == 2) {
    print("Please pay 3000");
  } else if (ticketType == 3) {
    print("Please pay 7000");
  } else {
    print("Please pay 2500");
  }
}
