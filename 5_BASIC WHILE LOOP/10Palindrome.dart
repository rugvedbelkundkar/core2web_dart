void main() {
  int n = 2332;
  int temp = n;
  int rem = 0;
  int rev = 0;
  while (n > 0) {
    rem = n % 10;
    rev = rem + (rev * 10);
    n = n ~/ 10;
  }
  //print("reverse of num is : $rev");
  if (temp == rev) {
    print("num is palindrome");
  } else {
    print("num is not palindrome");
  }
}
