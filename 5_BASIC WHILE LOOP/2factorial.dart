void main() {
  int n = 6;
  int num = n;
  int fact = 1;
  while (n > 0) {
    fact = fact * n;
    n--;
  }
  print("factorial of $num is $fact");
}
