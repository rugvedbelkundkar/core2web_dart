/*void main() {
  int num = 942111423;
  int rem = 0;
  int squareSum = 0;

  while (num > 0) {
    rem = num % 10; 
    if (rem % 2 == 0) {
      // Check if it's even
      squareSum += (rem * rem); 
    }
    num ~/= 10; // Remove the last digit
  }

  print(squareSum);
}
*/
void main() {
  int num = 942111423;
  int rem = 0;
  int squareSum = 0;

  while (num > 0) {
    rem = num % 10;
    if (rem % 2 == 0) {
      squareSum = (rem * rem);
      print(squareSum);
    }
    num ~/= 10;
  }

  
}
