void main() {
  int i = 1;
  int pro = 1;
  int sum = 0;
  while (i <= 10) {
    if (i % 2 == 1) {
      pro = pro * i;
    } else {
      sum = sum + i;
    }
    i++;
  }
  print("sum of even num between 1 to 10 : $sum");
  print("multiplication of even num between 1 to 10 : $pro");
}
