import 'dart:io';

void main() {
  stdout.write("Enter a number: ");
  int n = int.parse(stdin.readLineSync()!);

  while (n > 0) {
    if (n % 2 == 1) {
      stdout.write("$n ");
      n=n-2;
    } 
    else {
      stdout.write("$n ");
      n--;
    }
  }
}
