//  palindrome number

void main() {
  int n = 942111423;
  int rem = 0;
  int rev = 0;
  while (n > 0) {
    rem = n % 10;
    rev = rem + (rev * 10);
    n = n ~/ 10;
  }
  print("reverse of num is : $rev");
 
}
