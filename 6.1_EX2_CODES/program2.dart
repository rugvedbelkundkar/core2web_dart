/* 
print each digits of given numbers 
and count of the digits
*/

void main() {
  int n = 942;
  int n2 = 0;
  int cnt = 0;
  while (n > 0) {
    n2 = n % 10;
    print(n2);
    n = n ~/ 10;
    cnt++;
  }
  print("count is $cnt ");
}
