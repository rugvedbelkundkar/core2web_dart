// cont of even and odd digits of number

void main() {
  int n = 942111423;
  int temp = n;
  int cnteven = 0;
  int cntodd = 0;
  int n2 = 0;

  while (n > 0) {
    n2 = n % 10;
    if (n2 % 2 == 0)
      cnteven++;
    else
      cntodd++;
    n = n ~/ 10;
  }
  print("count of even digits of $temp is : $cnteven ");
  print("count of odd digits of $temp is : $cntodd ");
}
