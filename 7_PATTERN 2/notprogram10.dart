/*
1 3 5 7
3 6 9 12
6 10 14 18
10 15 20 25
*/

import 'dart:io';

void main() {
  stdout.write("Enter the number of rows: ");
  int n = int.parse(stdin.readLineSync()!);

  for (int i = 1; i <= n; i++) {
    int x = 1;
    for (int j = 1; j <= n; j++) {
      stdout.write("$x ");
      x = x + i;
    }
    x++;
    print("");
  }
}
