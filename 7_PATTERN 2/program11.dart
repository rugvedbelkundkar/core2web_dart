import 'dart:io';

void main() {
  int num = 4;
  for (int i = 0; i < 4; i++) {
    for (int j = 0; j < 5; j++) {
      if (i == 0 && j == 3) {
        stdout.write("${num + 1} ");
      } else if (i == 0 && j == 4) {
        stdout.write("${num + 2} ");
      } else if (i == 1 && j == 2) {
        stdout.write("${num + 2} ");
      } else if (i == 1 && j == 3) {
        stdout.write("${num + 3} ");
      } else if (i == 1 && j == 4) {
        stdout.write("${num + 4} ");
      } else if (i == 2 && j == 1) {
        stdout.write("${num + 4} ");
      } else if (i == 2 && j == 2) {
        stdout.write("${num + 5} ");
      } else if (i == 2 && j == 3) {
        stdout.write("${num + 7} ");
      } else if (i == 2 && j == 4) {
        stdout.write("${num + 8} ");
      } else if (i == 3 && j == 0) {
        stdout.write("${num + 8} ");
      } else if (i == 3 && j == 1) {
        stdout.write("${num + 9} ");
      } else if (i == 3 && j == 2) {
        stdout.write("${num + 10} ");
      } else if (i == 3 && j == 3) {
        stdout.write("${num + 12} ");
      } else if (i == 3 && j == 4) {
        stdout.write("${num + 13} ");
      } else {
        stdout.write("$num ");
      }
      num++;
    }
    num++;
    print("");
  }
}
