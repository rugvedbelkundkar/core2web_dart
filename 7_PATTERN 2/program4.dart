/*
5 6 7 8
6 7 8 9
7 8 9 10
8 9 10 11
*/
import 'dart:io';

void main() {
  stdout.write("enter no of rows : ");
  int n = int.parse(stdin.readLineSync()!);
  for (int i = 1; i <= n; i++) {
    int x = i + n;
    for (int j = 1; j <= n; j++) {
      stdout.write("$x ");
      x++;
    }
    print("");
  }
}
