/*
4 5 6
4 5 6
4 5 6
*/
import 'dart:io';

void main() {
  stdout.write("enter no of rows : ");
  int n = int.parse(stdin.readLineSync()!);
  for (int i = 1; i <= n; i++) {
    int x = n + 1;
    for (int j = 1; j <= n; j++) {
      stdout.write("$x ");
      x++;
    }
    print("");
  }
}
