/*
1 2 3 4
2 4 6 8
3 6 9 12
4 8 12 16
*/
import 'dart:io';

void main() {
  stdout.write("enter no of rows : ");
  int n = int.parse(stdin.readLineSync()!);
  for (int i = 1; i <= n; i++) {
    int x = i;
    for (int j = 1; j <= n; j++) {
      stdout.write("$x ");
      x = x + i;
    }
    print("");
  }
}
