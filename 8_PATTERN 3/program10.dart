/*
1
3 4
5 6 8
7 8 10 13
9 10 12 15 19
*/
import 'dart:io';

void main() {
  stdout.write("Enter the number of rows: ");
  int n = int.parse(stdin.readLineSync()!);
  for (int i = 1; i <= n; i++) {
    int n = i * 2 - 1;
    for (int j = 1; j <= i; j++) {
      stdout.write("$n ");
      n = n + j;
    }
    print("");
  }
}
