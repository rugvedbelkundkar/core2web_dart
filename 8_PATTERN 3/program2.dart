/*
1
1 1
1 1 1 
1 1 1 1
*/
import 'dart:io';

void main() {
  stdout.write("enter no of rows : ");
  int n = int.parse(stdin.readLineSync()!);
  for (int i = 1; i <= n; i++) {
    int x = 1;
    for (int j = 1; j <= i; j++) {
      stdout.write("$x ");
    }
    print("");
  }
}
