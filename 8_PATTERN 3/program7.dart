/*
10
9 8
7 6 5
4 3 2 1
*/
import 'dart:io';

void main() {
  stdout.write("enter no of rows : ");
  int n = int.parse(stdin.readLineSync()!);
  int x = n * (n + 1) ~/ 2;
  for (int i = 1; i <= n; i++) {
    for (int j = 1; j <= i; j++) {
      stdout.write("$x ");
      x--;
    }
    print(" ");
  }
}
