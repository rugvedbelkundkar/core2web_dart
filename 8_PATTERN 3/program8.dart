/*
5
6 8
7 10 13
8 12 16 20
9 14 19 24 29
*/

import 'dart:io';

void main() {
  stdout.write("enter no rows : ");
  int n = int.parse(stdin.readLineSync()!);
  int x = n;
  for (int i = 1; i <= n; i++) {
    x = n + i - 1;
    for (int j = 1; j <= i; j++) {
      stdout.write('$x ');
      x = x + i;
    }
    print("");
  }
}
