/*
1 1 1 1
2 2 2 
3 3
4
*/

import 'dart:io';

void main() {
  stdout.write("enter no of rows : ");
  int n = int.parse(stdin.readLineSync()!);
  for (int i = 1; i <= n; i++) {
    for (int j = 1; j <= n-i+1; j++) {
      stdout.write("$i ");
    }
    print("");
  }
}
