/*
4 3 2 1
4 3 2
4 3
4
*/

import 'dart:io';

void main() {
  stdout.write("enter no of rows : ");
  int n = int.parse(stdin.readLineSync()!);
  for (int i = 1; i <= n; i++) {
    int x = n;
    for (int j = 1; j <= n - i + 1; j++) {
      stdout.write("$x ");
      x--;
    }
    print("");
  }
}
