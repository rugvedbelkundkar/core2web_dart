/*
2  4  6  8
10 12 14 
16 18 
20
*/

import 'dart:io';

void main() {
  stdout.write("enter no of rows : ");
  int n = int.parse(stdin.readLineSync()!);
  int x = 2;
  for (int i = 1; i <= n; i++) {
    for (int j = 1; j <= n - i + 1; j++) {
      stdout.write("$x ");
      x=x+2;
    }
    print("");
  }
}
