/*
4 8 12 16
4 7 10
4 6
4
*/

import 'dart:io';

void main() {
  stdout.write("Enter the number of rows: ");
  int n = int.parse(stdin.readLineSync()!);
  int temp = n;
  for (int i = 1; i <= n; i++) {
    int x = n;
    for (int j = 1; j <= n - i + 1; j++) {
      stdout.write("$x ");
      x = x + temp;
    }
    print("");
    temp--;
  }
}
