/*
1 2 3 4
1 3 5
1 4
1
*/

import 'dart:io';

void main() {
  stdout.write("enter no of rows : ");
  int n = int.parse(stdin.readLineSync()!);
  for (int i = 1; i <= n; i++) {
    int x = 1;
    for (int j = 1; j <= n - i + 1; j++) {
      stdout.write("$x ");
      x = x + i;
    }
    print("");
  }
}
