void fun1() {
  for (int i = 0; i < 5; i++) print('fun1');
}

void fun2() {
  for (int i = 0; i < 5; i++) {
    print('fun2');
    Future.delayed(Duration(seconds: 5), () => fun2());
  }
  for (int i = 0; i < 3; i++) {
    print('delayed fun2');
  }
}

void main() {
  print('in main');
  fun1();
  fun2();
  print('end main');
}
