

abstract class developer {
  void develop() {
    print("we develop software");
  }

  void devType();
}

class mobileDev extends developer {
  void devType() {
    print("flutter developer");
  }
}

class webDev extends developer {
  void devType() {
    print("forntend dev");
  }
}

void main() {
  developer obj1 = new mobileDev();
  obj1.develop();
  obj1.devType();

  developer obj2 = new webDev();
  obj2.develop();
  obj2.devType();

  webDev obj3 = new webDev();
  obj3.develop();
  obj3.devType();

  /*developer obj4 = new developer();
  obj4.develop();
  obj4.devType();    error */
}
