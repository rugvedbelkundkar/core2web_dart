void main() {
  var progLang = ["cpp", "java", "python", "dart"];
  print(progLang); //[cpp, java, python, dart]
  print(progLang.runtimeType); //List<String>
  print(progLang[0]); // cpp
  print(progLang[2]); // python
  print(progLang[3]); // dart

  List data = ["cpp", "java"];
  print(data.runtimeType); // List<dynamic>

  var edata = [10, "kanha", "bmc", 1.5];
  print(edata);
  edata[3] = 3.0;
  print(edata); //mutable [10, kanha, bmc, 3.0]
}
