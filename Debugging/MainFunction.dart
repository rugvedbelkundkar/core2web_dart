// main.dart
import 'dart:io';
import 'PalindromeFunc.dart';

void main() {
  stdout.write('Enter the first number:');
  int n1 = int.parse(stdin.readLineSync()!);
  stdout.write('Enter the second number:');
  int n2 = int.parse(stdin.readLineSync()!);

  int count = 0;

  for (int i = n1; i <= n2; i++) {
    if (i == revnum(i)) {
      print(i);
      count++;
    }
  }

  print('The total count of palindrome numbers between $n1 and $n2 is: $count');
}
