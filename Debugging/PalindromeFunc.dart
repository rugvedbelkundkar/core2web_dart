// PalindromeFunc.dart
int revnum(int n) {
  int temp = n;
  int reversed = 0;

  while (temp > 0) {
    int digit = temp % 10;
    reversed = (reversed * 10) + digit;
    temp = temp ~/ 10;
  }

  return reversed;
}
