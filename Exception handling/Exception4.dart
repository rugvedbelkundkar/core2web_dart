import 'dart:io';

void main() {
  print("start main");
  print("enter value : ");
  try {
    int? n = int.parse(stdin.readLineSync()!);
    print(n);
  } on IntegerDivisionByZeroException {
    print(" In Integer Division By Zero Exception");
  } on FormatException {
    print("FormatException");
  } catch (ex) {
    print(ex);
  }
  print("end main");
}
