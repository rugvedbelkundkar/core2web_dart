import 'dart:io';

void main() {
  print("start main");
  print("enter value : ");
  try {
    int val = int.parse(stdin.readLineSync()!); //10
    print(val); //10
  } catch (ex) {
    print(ex);
  }
  print("end main");
}
/*
input=rugved
output : Error- 
FormatException: Invalid radix-10 number (at character 1)
rugved
^
*/