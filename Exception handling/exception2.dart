import 'dart:io';

void main() {
  print("start main");
  print("enter a value : ");
  try {
    int? n = int.parse(stdin.readLineSync()!);
    print(n);
  } on FormatException {
    print('Exception Handled');
  } catch (ex) {
    print(ex);
  }
  print("end main");
}
