import 'dart:io';

void main() {
  print("start main");
  print("enter value : ");
  try {
    int? n = int.parse(stdin.readLineSync()!);
    print(n);
  } catch (ex) {
    print("here and there");
  } on IntegerDivisionByZeroException {
    print("Exception Handled");
  } on FormatException {
    print("FormatException");
  }
  print("end main ");
}
