class demo {
  void fun() {
    print("in fun");
  }
}

void main() {
  demo obj = new demo();
  obj.fun(); // in fun

  /*obj = null;
  obj.fun(); // Error: The value 'null' can't be assigned to a variable of type 'demo' because 'demo' is not nullable.*/
}
