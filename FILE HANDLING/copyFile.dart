import 'dart:io';

void main() {
  File f1 = new File('c2w.txt');
  File f2 = new File('xyz.txt');

  f1.copy(f2.path); // copy requires stirng type so we give path
  String data = f1.readAsStringSync().substring(0, 10);
  print(data);
}
