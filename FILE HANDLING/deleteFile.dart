import 'dart:io';

void main() async {
  File f1 = new File('xyz.txt');
  if (f1.existsSync()) {
    f1.delete(); // file will delete
    print('file deleted');
  } else {
    print('file no found');
  }
}
