import 'dart:io';

void main() async {
  File f = new File('c2w.txt');
  print(f.runtimeType);

  await //f.create();
   f.create();
  print('file created');
  print(f.absolute);//File: 'D:\Visual Studio\Core2Web\DART\FILE HANDLING\c2w.txt'
  print(f.path);//c2w.txt
  print(f.lastAccessed());//Instance of 'Future<DateTime>'
  print(f.lastModified());//Instance of 'Future<DateTime>'
  print(f.length());//Instance of 'Future<int>'
  print(f.exists());//Instance of 'Future<bool>'
  print(f.lengthSync());//73
  print(f.existsSync());//true
}
