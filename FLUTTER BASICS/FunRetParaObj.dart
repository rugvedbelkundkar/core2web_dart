class Demo {
  final int x;
  final String str;
  const Demo(this.x, this.str);
  void fun() {
    print('in fun');
    print(x);
    print(str);
  }
}

Demo objFun() {
  print('in objFun');
  return Demo(10, 'kanha');
}

void main() {
  Demo obj = objFun();
  obj.fun();
}
/*
in objFun
in fun
10
kanha
*/