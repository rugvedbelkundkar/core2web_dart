

class Demo {
  Demo() {
    print('in constructor');
  }
  void fun() {
    print('in fun');
  }
}

Demo objFun() {
  print('in objFun');
  return Demo();
}

void main() {
  Demo Obj = objFun();
  Obj.fun();
}
/*
in objFun
in constructor
in fun
*/