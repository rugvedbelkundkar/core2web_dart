class demo {
  demo() {
    print('in constructor');
  }
  void fun() {
    print('in fun method');
  }
}

void objFun(demo obj) {
  print('in objFun');
  obj.fun();
}

void main() {
  demo obj = new demo();
  objFun(obj);
}
/*
in constructor
in objFun
in fun method
*/