class demo {
  demo._private() {
    print("In constructor");
  }
  factory demo() {
    print("In factory constructor");
    return demo._private();
  }
}
// we can write main fuction in another file also
void main() {
  new demo();
  
}


