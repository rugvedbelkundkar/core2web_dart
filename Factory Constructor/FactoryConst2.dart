class demo {
  demo._private() {
    print("In constructor");
  }
  factory demo() {
    print("In factory constructor");
    demo obj = new demo._private();
    return obj;
  }
  void fun() {
    print("in fun");
  }
}

// we can write main fuction in another file also
void main() {
  demo obj =new demo();
  obj.fun();
}
