import 'FactoryConst3.dart';

void main() {
  backend obj1 = new backend('JavaScript');
  print(obj1.lang);
  backend obj2 = new backend("Java");
  print(obj2.lang);
  backend obj3 = new backend("Python");
  print(obj3.lang);
}
