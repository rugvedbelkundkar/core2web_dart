abstract class Developer {
  factory Developer(String devType) {
    if (devType == "Backend") {
      return Backend();
    } else if (devType == "FrontEnd") {
      return FrontEnd();
    } else if (devType == "Mobile") {
      return Mobile();
    } else {
      return Other();
    }
  }
  void devLang();
}

class Backend implements Developer {
  void devLang() {
    print("NodeJS/SpringBoot");
  }
}

class FrontEnd implements Developer {
  void devLang() {
    print("ReactJS/Angulas JS");
  }
}

class Mobile implements Developer {
  void devLang() {
    print("Flutter/Android/Kotlin");
  }
}

class Other implements Developer {
  void devLang() {
    print("Testing/Devops/Support");
  }
}

void main() {
  Developer obj1 = new Developer("Forntend");
  obj1.devLang();
  Developer obj2 = new Developer("Backend");
  obj2.devLang();
  Developer obj3 = new Developer("Mobile");
  obj3.devLang();
  Developer obj4 = new Developer("Testing");
  obj4.devLang();
}
