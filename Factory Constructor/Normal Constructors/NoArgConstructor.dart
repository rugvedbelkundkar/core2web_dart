class demo {
  demo._private() {
    print("in private constructor");
  }
  demo() {
    print("in constructor");
  }
}

void main() {
  new demo();
  demo._private();
}
