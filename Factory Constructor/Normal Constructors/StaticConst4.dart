class demo {
  static demo obj = new demo();
  static int? x = 10;
  demo() {
    print("in constructor"); //output
  }
  demo fun() {
    return obj;
  }
}

void main() {
  new demo();
  print(demo.x);
 
}
