class demo {
  final int? x;
  final String? str;
  const demo(this.x, this.str);
  /*{
    print("constant constructor"); //Error: A const constructor can't have a 
                      body.Try removing either the 'const' keyword or the body.
  }*/
}

void main() {
  demo obj1 = new demo(10, "kanha");
  demo obj2 = new demo(20, "shashi");

  print(obj1.hashCode);
  print(obj2.hashCode);
  print(obj1.x);
  print(obj2.str);
}
