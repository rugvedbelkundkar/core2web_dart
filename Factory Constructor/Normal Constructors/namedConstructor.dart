class demo {
  demo() {
    print("named constructor");
  }
  demo.one() {
    print("named constructor one");
  }
  demo.two() {
    print("named constructor two");
  }
}

void main() {
  new demo();
  new demo.one();
  new demo.two();
}
