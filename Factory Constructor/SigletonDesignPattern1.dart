class demo {
  static demo obj = new demo._private();

  demo._private() {
    print("In constructor");
  }
  factory demo() {
    return obj;
  }
}
