class parent {
  parent() {
    print("parent constructor");
  }
}

class child1 extends parent {
  child1() {
    print("child constructor");
  }
}

class child2 extends parent {
  child2() {
    print("child constructor");
  }
}

void main() {
  new parent();
  new child1();
  new child2();
}
