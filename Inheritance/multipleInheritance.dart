class parent {}

class parent2 {}

/*class child extends parent,parent2{
  ERROR
}*/
void main() {}
/*Error: Each class definition can have at most one extends clause.
Try choosing one superclass and define your class to implement (or mix in) the others.*/