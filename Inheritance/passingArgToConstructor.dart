
class company {
  String? compName;
  String? Loc;

  company(this.compName, this.Loc);
  void compInfo() {
    print(compName);
    print(Loc);
  }
}

class Employee extends company {
  int empId;
  String? empName;
  Employee(this.empId, this.empName, String compName, String Loc)
      : super(compName, Loc);
  void empInfo() {
    print("Employee Id: $empId");
    print("Employe Name: $empName");
  }
}

void main() {
  Employee obj = new Employee(25, "Rugved", "TCS", "Pune");
  obj.empInfo();
  obj.compInfo(); 
}
