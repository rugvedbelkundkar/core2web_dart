class parent {
  int x = 10;
  String str = "Rugved";

  void parentDisp() {
    print("In parent Disp");
  }
}

class child extends parent {}

void main() {
  child obj = new child();
  print(obj.x);
  print(obj.str);

  obj.parentDisp();
}
