class parent {
  int x = 10;
  String str = "Rugved";

  void parentMethod() {
    print(x);
    print(str);
  }
}

class child extends parent {
  int y = 20;
  String str2 = "data";

  void childMethod() {
    print(y);
    print(str2);
  }
}

void main() {
  /*parent obj1 = new parent();
  print(obj1.y);
  print(obj1);
  obj1.childMehtod();*/

  /*parent obj2 = new parent();
  print(obj2.x);
  print(obj2.str);
  obj2.parentMethod();*/

  child obj2 = new child();
  /*print(obj2.x);
  print(obj2.str);
  obj2.parentMethod();*/

  print(obj2.y);
  print(obj2.str2);
  obj2.childMethod();
}
