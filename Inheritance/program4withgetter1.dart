class parent {
  int x = 10;
  String str = "Rugved";

  get getX => x;
  get getStr => str;
}

class child extends parent {
  int y = 20;
  String str2 = "Jignesh";

  get getY => y;
  get getStr2 => str2;
}

  void main() {
  child obj = new child();
  /* print(obj.getX);
  print(obj.getStr);
  print(obj.getY);
  print(obj.getStr2);*/

  print(obj.x);
  print(obj.str);
  print(obj.y);
  print(obj.str2);
}