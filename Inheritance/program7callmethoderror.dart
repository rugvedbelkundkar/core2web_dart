class parent {
  parent() {
    print("parent constructor");
  }
}

class child extends parent {
  child() {
    //super(); //Error: Superclass has no method named 'call'
    print("child constructor");
  }
}

void main() {
  /*child obj =*/ new child();
}
