class parent {
  parent() {
    print("parent constructor");
  }
  call() {
    print("in call method");
  }
}

class child extends parent {
  child() {
    super(); //Error: Superclass has no method named 'call'
    print("child constructor");
  }
}

void main() {
  child obj = new child();
  obj.call();
}
