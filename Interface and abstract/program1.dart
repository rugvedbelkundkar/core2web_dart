abstract class developer {
  int x = 10;
  developer() {
    print("dev constructor");
  }
  void develop() {
    print("we build software");
  }

  void devType();
}

class mobileDev implements developer {
  int x = 20;
  mobileDev() {
    print("mobileDev constructor");
  }
  void develop() {
    print("we build mobile apps");
  }

  void devType() {
    print("flutter developer");
  }
}

void main() {
  developer obj = new mobileDev();
  obj.develop();
  obj.devType();
  print(obj.x);
}
