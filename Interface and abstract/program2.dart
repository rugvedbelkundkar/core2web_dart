abstract class IFC {
  void material() {
    print("Indian material");
  }

  void taste();
}

class IndianFC implements IFC {
  void material() {
    print("Indian material");
  }

  void taste() {
    print("Indian taste");
  }
}

class EuFC {
  void material() {
    print("Indian material");
  }

  void taste() {
    print("europian taste");
  }
}

void main() {
  IndianFC obj = new IndianFC();
  obj.material();
  obj.taste();

  EuFC obj2 = new EuFC();
  obj2.material();
  obj2.taste();


}
