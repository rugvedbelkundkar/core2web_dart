mixin demo {
  void fun1() {
    print("in fun1");
  }

  void fun2();
}

class asach {
  void ashi() {
    print("in ashi method");
  }
}

class child extends asach with demo {
  void fun2() {
    print("in fun2");
  }
}

void main() {
  demo obj = new child();
  obj.fun1();
  obj.fun2();
  //obj.ashi(); // Error: The method 'ashi' isn't defined for the class 'demo'.
}
