mixin DemoParent {
  void m1() {
    print("in M1 - DemoParent");
  }
}
class Demo {
  void m2() {
    print("in M2-Demo");
  }
}

/*class DemoChild extends Demo, DemoParent {}
ERROR -  Error: Each class definition can have at most one extends clause.
Try choosing one superclass and define your class to implement (or mix in) the others.
*/
class DemoChild extends Demo with DemoParent {}// if , instead with the error

void main() {
  DemoChild obj = new DemoChild();
  obj.m1();
  obj.m2();
}
