mixin demo1 {
  void fun1() {
    print("in fun1 demo1");
  }
}
mixin demo2 on demo1 { 
  void fun2() {
    print("in fun2 demo2");
  }
}

class demoChild with demo1, demo2 {}

void main() {
  demoChild obj = new demoChild();
  obj.fun1(); // prints "in fun1 demo1"
  obj.fun2(); // prints "in fun2 demo2"
}

