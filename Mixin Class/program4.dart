

abstract class demo1 {
  void fun1() {
    print("in demo fun1");
  }

  void fun2();
}

abstract class demo2 {
  void fun3() {
    print("in demo2 fun3");
  }

  void fun4();
}

class demoChild implements demo1, demo2 {
  void fun2() {
    print("in demoChild fun2");
  }

  void fun4() {
    print("in demoChild fun4");
  }
}

/*Error: The non-abstract class 'demoChild' is missing implementations for these members:
- demo1.fun1
- demo2.fun3
Try to either- provide an implementation,
- inherit an implementation from a superclass or mixin, 
- mark the class as abstract, or
- provide a 'noSuchMethod' implementation.*/
void main() {
  demoChild obj = new demoChild();
  //obj.fun1(); // prints "in demoChild fun4"
  obj.fun2(); // prints "in demoChild fun2"
  //obj.fun3(); // prints "in demo2 fun3"
  obj.fun4(); // prints "in demoChild fun4"

}
