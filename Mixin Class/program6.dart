mixin demo1 {
  demo1(){ //Error: Mixins can't declare constructors
    print("in constructor");
  }
  void fun1() {
    print("fun 1 demo 1");
  }
  void fun2();
}
void main() {
  demo1 obj = new demo1(); //Error: The class 'demo1' is abstract and can't be   
                          //instantiated.
}
