mixin demo1 {
  int x = 10;
  void fun1() {
    print("in fun1 demo1");
  }

  void fun2();
}

class demoChild with demo1 {
  void fun2() {
    print("in demochild fun2");
    print("$x");
  }
}

void main() {
  demoChild obj = new demoChild();
  obj.fun1(); // prints "in fun1 demo1"
  obj.fun2(); // prints "in demochild fun2",10

}
