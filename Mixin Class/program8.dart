
mixin demo1 {
  int x = 10;
  void fun1() {
    print("in fun1 demo1");
  }

  void fun2();
}

class demoChild extends demo1 {
  void fun2() {
    print("in demoChild fun2");
    print("$x");
  }
}

void main() {
  demoChild obj = new demoChild(); // Error: The superclass, 'demo1', has no     
                                // unnamed constructor that takes no arguments.
  obj.fun1(); //accessing the method of mixin
  obj.fun2(); //accessing the method of class
}
