mixin demo1 {
  void fun1() {
    print("in fun1 demo1");
  }

  void fun2();
}
mixin demo2 {}

class demoChild with demo1, demo2 {}
/*Error: The non-abstract class 'demoChild' is missing implementations for these members:
 - demo1.fun2
 */

void main() {
  demo1 obj = new demoChild();
  obj.fun1(); // this will call the method of demo1 class
  obj.fun2(); // this will give error as it is not defined in demo1 or demo2
}
