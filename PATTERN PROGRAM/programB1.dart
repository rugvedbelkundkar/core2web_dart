import 'dart:io';

int n = 5;
void main() {
  for (; n >= 1; n--) {
    for (int j = 1; j <= n; j++) {
      stdout.write("*");
    }
    print("");
  }
}
