import 'dart:io';

int n = 5;
int num = 1;
void main() {
  for (int i = 1; i <= n; i++) {
    for (int s = 1; s <= n - i; s++) {
      stdout.write(" ");
    }
    for (int j = 1; j <= i; j++) {
      stdout.write(num);
    }
    num++;
    print("");
  }
}
