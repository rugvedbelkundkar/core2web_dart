import 'dart:io';

void main() {
  int n = 5;
  for (int i = 1; i <= 5; i++) {
    for (int s = 1; s < i; s++) {
      stdout.write(" ");
    }
    for (int j = 1; j <= n + 1 - i; j++) {
      stdout.write(j);
    }
    print("");
  }
}
