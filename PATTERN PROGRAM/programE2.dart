import 'dart:io';

int n = 5;
void main() {
  for (int i = 1; i <= n; i++) {
    for (int s = 1; s <= n - i; s++) {
      stdout.write(" ");
    }
    for (int j = 1; j <= 2 * i - 1; j++) {
      stdout.write(j);
    }
    print("");
  }
}
