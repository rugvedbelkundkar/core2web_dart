import 'dart:io';

void main() {
  int n = 5;
  for (int i = 1; i <= n; i++) {
    for (int s = 1; s <= n - i; s++) {
      stdout.write(" ");
    }
    for (int j = 1; j <= 2 * i - 1; j++) {
      stdout.write("*");
    }
    print("");
  }
  n--;
  for (int i = 1; i <= n; i++) {
    for (int s = 1; s <= i; s++) {
      stdout.write(" ");
    }
    for (int j = 1; j <= 2 * (n - i) + 1; j++) {
      stdout.write("*");
    }
    print("");
  }
}
