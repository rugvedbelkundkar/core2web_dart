class parent {
  void career() {
    print("engineering");
  }

  void marry() {
    print("dipika");
  }
}

class child extends parent {
  void marry() {
    print('disha patani');
  }
}

void main() {
  child obj = new child();
  obj.career();
  obj.marry();
}
