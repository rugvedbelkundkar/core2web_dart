class demo {
  int y = 20;
  int x = 10;
  void disp(int x) {
    this.x = x;
    print(x);
  }

  /*void disp(int x, int y) { //Error: 'disp' is already declared in this scope.
    this.x = x;
    this.y = y;
    print(x);
    print(y);
  }*/
}

void main() {
  demo obj = new demo();
  obj.disp(10);
  //obj.disp(10,20); //Error: Too many positional arguments: 1 allowed, but 2 found.Try removing the extra positional arguments.
}
