

abstract class IFC {
  void material() {
    print("Indian Material");
  }

  void taste();
}

class IndianFC implements IFC {
  void material() {
    print("Indian Material");
  }

  void taste() {
    print("Indian taste");
  }
}

class euFC extends IFC {
  void material() {
    print("Indian matrial");
  }

  void taste() {
    print("Europian taste");
  }
}

void main() {
  IndianFC obj = new IndianFC();
  obj.material();
  obj.taste();
}
