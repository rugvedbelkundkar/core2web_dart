

mixin DemoParent {
  void m1() {
    print("in M1 - DemoParent");
  }
}

class Demo {
  void m2() {
    print("in M2-Demo");
  }
}

class DemoChild extends Demo with DemoParent {}

void main() {
  DemoChild obj = new DemoChild();
  obj.m1();
  obj.m2();
}
