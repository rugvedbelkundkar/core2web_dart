mixin DemoParent {
  void m1() {
    print("In M1 -Department");
  }
}
mixin Demo {
  void m1() {
    print("In m1 - Demo");
  }
}
class DemoChild implements with Demo,DemoParent{

}
void main(){
  DemoChild obj=new DemoChild();
  obj.m1();
}
