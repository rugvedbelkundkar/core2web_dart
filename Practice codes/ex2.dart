void main() {
  int i = 0;
  while (i < 100) {
    if (i % 4 == 0 || i % 5 == 0) {
      i++; // Increment i before continuing
      continue;
    }
    print(i);
    i++;
  }
}
