class getter {
  int? _x;
  String? str;
  double? _sal;

  getter(this._x, this.str, this._sal);

  get getX => _x;
  get getSal => _sal;
  get getStr => str;

  void disp() {
    print(_x);
    print(str);
    print(_sal);
  }
}
