// prime number
void main() {
  int n = 1021, count = 0;

  for (int i = 1; i <= n; i++) {
    if (n % i == 0) {
      count++;
    }
    i++;
  }
  if (count == 2)
    print("$n is prime number");
  else
    print("$n is not a prime number");
}
