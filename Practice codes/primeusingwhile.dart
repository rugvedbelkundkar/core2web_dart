// prime number
void main() {
  int n = 17, count = 0, i = 1;
  while (i <= n) {
    if (n % i == 0) count++;
    i++;
  }
  if (count == 2)
    print("$n is prime number");
  else
    print("$n is not a prime number");
}
