import 'dart:io';

void fun(int x) {
  if (x == 0) return;
  print(x);
  x--;
  fun(x);
}

void main() {
  stdout.write("enter number : ");
  int? num = int.parse(stdin.readLineSync()!);
  fun(num);
}
