import 'dart:io';

void fun(int x) {
  if (x == 0) return;
  print(x);
  x--;
  fun(x);
}

void main() {
  int? a;
  stdout.write("Enter a number : ");
  a = int.parse(stdin.readLineSync()!);
  fun(a);
}
