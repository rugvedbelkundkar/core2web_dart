import 'dart:io';

int s = 0;
void fun(int x) {
  if (x == 0) return;
  s = s + x;
  x--;
  fun(x);
}

void main() {
  int? a;
  stdout.write("Enter a number : ");
  a = int.parse(stdin.readLineSync()!);
  fun(a);
  print(s);
}
