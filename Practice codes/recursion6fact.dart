import 'dart:io';

int f = 1;
void fact(int n) {
  if (n == 0) return;
  f = f * n;
  n--;
  fact(n);
}

void main() {
  int? a;
  stdout.write("Enter a number : ");
  a = int.parse(stdin.readLineSync()!);
  fact(a);
  stdout.write("\nFactorial of $a is : $f");
}
