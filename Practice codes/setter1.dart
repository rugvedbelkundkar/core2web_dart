class setter {
  int? _x;
  String? str;
  double? _sal;

  setter(this._x, this.str, this._sal);

  set setX(int x) => _x = x;
  set setName(String Name) => str = Name;
  set setSal(double Sal) => _sal = Sal;

  void display() {
    print(_x);
    print(str);
    print(_sal);
  }
}
