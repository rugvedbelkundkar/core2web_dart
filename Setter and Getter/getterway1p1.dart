class demo {
  int? _x;
  String? str;
  double? _sal;

  demo(this._x, this.str, this._sal);

  int? getX() {
    return _x;
  }

  double? getSal() {
    return _sal;
  }
}
