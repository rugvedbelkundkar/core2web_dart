class demo {
  int? _x;
  String? str;
  double? _sal;

  demo(this._x, this.str, this._sal);

  int? get getX {
    return _x;
  }

  double? get getSal {
    return _sal;
  }
}
