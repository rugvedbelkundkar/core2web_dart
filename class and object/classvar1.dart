class demo {
  int x = 10;
  static int y = 20;

  void printData() {
    print(x);
    print(y);
  }
}

void main() {
  demo obj = new demo();
  obj.printData();

  demo obj2 = new demo();
  obj2.x = 15;
  //obj2.y = 25; // error
  obj2.printData();
}
