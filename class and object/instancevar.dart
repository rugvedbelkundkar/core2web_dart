class Company {
  int empCount = 500;
  String compName = "Google";
  String loc = "Pune";

  void compInfo() {
    print(empCount);
    print(compName);
    print(loc);
  }
}

void main() {
  Company obj = new Company();
  obj.compInfo();

  // we can create objects in four ways 

  /*Company obj2 = Company();
  obj2.compInfo();

  new Company();
  new Company().compInfo();

  Company();
  Company().compInfo();
  */
}
