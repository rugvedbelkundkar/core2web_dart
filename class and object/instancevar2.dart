class Employee {
  int empId = 10;
  String empName = "Rugved";
  double empSal = 1.2;

  void empInfo() {
    print(empId);
    print(empName);
    print(empSal);
  }
}

void main() {
  Employee obj = new Employee();
  obj.empInfo();

  obj.empSal = 1.5;
  obj.empInfo();
}
