import 'dart:io';

class emp {
  int? empId = 11;
  String? empName = "Rugved";
  double? empSal = 1.2;

  void empInfo() {
    print(empId);
    print(empName);
    print(empSal);
  }
}

void main() {
  emp obj = new emp();
  obj.empInfo();

  stdout.write("enter emp id : ");
  obj.empId = int.parse(stdin.readLineSync()!);

  stdout.write("enter emp name : ");
  obj.empName = stdin.readLineSync();

  stdout.write("enter emp sal : ");
  obj.empSal = double.parse(stdin.readLineSync()!);

  obj.empInfo();
}
