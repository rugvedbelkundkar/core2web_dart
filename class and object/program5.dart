class demo {
  int? x;
  String? str;
  void printData() {
    print(x);
    print(str);
  }
}

void main() {
  demo obj = new demo();
  obj.printData();

  demo obj2 = new demo();
  obj2.x = 10;
  obj2.str = "rugved";
  obj2.printData();
}
