class Company {
  int? empCount;
  String? compName;

  Company(this.empCount, {this.compName = 'VI'});
  void compInfo() {
    print(empCount);
    print(compName);
  }
}

void main() {
  Company obj1 = new Company(500);
  //Company obj2 = new Company(200, "TCS");//error
  Company obj2 = new Company(200);

  obj1.compInfo();
  obj2.compInfo();
}
