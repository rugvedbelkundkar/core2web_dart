import 'dart:io';

void main() {
  final int? x;
  stdout.write("enter value of x : ");
  x = int.parse(stdin.readLineSync()!);

  print(x);
  /*
  x = 20;
  print(x); //error
  */
}
