class employee {
  int? empId;
  String? empName;

  employee() {
    print("default constructor");
  }

  employee.constr(int empId, String empName) {
    print("para const");
  }
}

void main() {
  employee obj1 = new employee();
  employee obj2 = new employee.constr(10, "Rugved");
  print(obj2.empId);//null
  print(obj2.empName);//null
  print(obj1.empId);//null
  print(obj1.empName);//null
}
