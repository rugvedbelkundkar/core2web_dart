class Company {
  int? empCount;
  String? compName;

  Company({this.empCount, this.compName});

  void compInfo() {
    print(empCount);
    print(compName);
  }
}

void main() {
  Company obj1 = new Company(empCount: 500, compName: "cognizent");
  Company obj2 = new Company(compName: "Google", empCount: 200);

  obj1.compInfo();
  obj2.compInfo();
}
