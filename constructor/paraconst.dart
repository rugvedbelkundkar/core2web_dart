class demo {
  int? x;
  String? str;

  demo(int x, String str) {
    print("in parameterized constructor");
    this.x = x;
    this.str = str;
  }
  void printData() {
    print(x); //null
    print(str); //null
  }
}

void main() {
  demo obj = new demo(10, "rugved");
  obj.printData();
}
