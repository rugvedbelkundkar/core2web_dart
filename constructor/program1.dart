class demo {
  int x = 10;
  demo() {
    print("constructor");
  }
}

void main() {
  demo obj = new demo();
  obj.x = 100;
  print(obj.x);
}
