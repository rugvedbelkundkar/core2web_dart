class demo {
  int x = 10;
  /*void*/ demo() {
    // error
    //Error: Constructors can't have a return type.
    //Try removing the return type.
    print("constructor");
  }
}

void main() {
  demo obj = new demo();
  obj.x;
}
